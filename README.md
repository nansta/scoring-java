# Scoring Program
Command line utility that will compute a score for a list of first names. The list of names will be provided as a text file. The full path to the names file will be specified as a command line argument. The names file will contain a single line of quoted, comma-separated names.



## Requirements
1. Java OpenJDK 11.0.6
2. JUnit 4.12

## Run
```
java -cp /path/to/target/classes com.occ.scoring.App /path/to/names.txt
```