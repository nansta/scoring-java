package com.occ.scoring;

import org.junit.Before;
import org.junit.Test;

import com.occ.scoring.name.NameScorer;
import com.occ.scoring.rulebook.AlphabetRuleBook;
import com.occ.scoring.rulebook.RuleBookFactory;
import com.occ.scoring.scorable.Scorable;
import com.occ.scoring.scorable.ScorableString;

import static org.junit.Assert.assertTrue;

/**
 * Unit tests for name scoring class.
 */
public class AppTest {
    AlphabetRuleBook rules = new AlphabetRuleBook();

    @Before
    public void populateRules() {
        final RuleBookFactory factory = new RuleBookFactory();

        this.rules = factory.getRuleBook("ALPHA");
    }

    private String message(int actualScore, int expectedScore) {
        return String.format("Score of %s should be %s", actualScore, expectedScore);
    }

    @Test
    public void testThatScoreSetsScoreForShortListOfNames() {
        int expectedScore = 3194;

        final String[] testNames = {
            "MARY", "PATRICIA", "LINDA", "BARBARA",
            "VINCENZO", "SHON", "LYNWOOD", "JERE", "HAI",
        };

        NameScorer tester = new NameScorer(this.rules);

        for (final String name : testNames) {
            tester.append(name);
        }

        tester.score();

        int actualScore = tester.getScore();

        assertTrue(this.message(actualScore, expectedScore), actualScore == expectedScore);
    }

    @Test
    public void testThatScoreSetsScoreForLongListOfNames() {
        int expectedScore = 89001;

        final String[] testNames = {
            "MARY", "PATRICIA", "LINDA", "BARBARA", "ELIZABETH", "JENNIFER", "MARIA", "SUSAN",
            "MARGARET", "DOROTHY", "LISA", "NANCY", "KAREN", "BETTY", "HELEN", "SANDRA", "DONNA",
            "CAROL", "RUTH", "SHARON", "MICHELLE", "LAURA", "SARAH", "KIMBERLY", "DEBORAH", "JESSICA",
            "SHIRLEY", "CYNTHIA", "ANGELA", "MELISSA", "BRENDA", "AMY", "ANNA", "REBECCA", "VIRGINIA",
            "KATHLEEN", "PAMELA", "MARTHA", "DEBRA", "AMANDA", "STEPHANIE", "CAROLYN", "CHRISTINE",
            "MARIE", "JANET", "CATHERINE", "FRANCES", "ANN", "JOYCE", "DIANE", "ALICE", "JULIE",
        };

        NameScorer tester = new NameScorer(this.rules);

        for (final String name : testNames) {
            tester.append(name);
        }

        tester.score();

        int actualScore = tester.getScore();

        assertTrue(this.message(actualScore, expectedScore), actualScore == expectedScore);
    }

    @Test
    public void testThatScoreItemReturnsScoreForLinda() {
        int expectedNameScore = 40;

        Scorable<String> name = new ScorableString("LINDA");

        NameScorer tester = new NameScorer(this.rules);

        int actualNameScore = tester.scoreItem(name);

        assertTrue(this.message(actualNameScore, expectedNameScore), actualNameScore == expectedNameScore);
    }

    @Test
    public void testResetScore() {
        int expectedScore = 385;

        final String[] testNames = { "MARY", "PATRICIA", "LINDA" };
        NameScorer tester = new NameScorer(this.rules);

        for (final String name : testNames) {
            tester.append(name);
        }

        tester.score();

        tester.score();

        int actualScore = tester.getScore();

        assertTrue(this.message(actualScore, expectedScore), actualScore == expectedScore);
    }

    @Test
    public void testSouldHandleStringsWithDoubleQuotes() {
        int expectedScore = 385;

        final String[] testNames = { "\"MARY\"", "\"PATRICIA\"", "\"LINDA\"" };
        NameScorer tester = new NameScorer(this.rules);

        for (final String name : testNames) {
            tester.append(name);
        }

        tester.score();

        tester.score();

        int actualScore = tester.getScore();

        assertTrue(this.message(actualScore, expectedScore), actualScore == expectedScore);
    }

    @Test
    public void testSouldHandleFileWithNewLine() {
        int expectedScore = 385;

        final String[] testNames = { "\"MARY\"", "\"PATRICIA\"", "\"LINDA\"\n" };
        NameScorer tester = new NameScorer(this.rules);

        for (final String name : testNames) {
            tester.append(name);
        }

        tester.score();

        tester.score();

        int actualScore = tester.getScore();

        assertTrue(this.message(actualScore, expectedScore), actualScore == expectedScore);
    }

}
