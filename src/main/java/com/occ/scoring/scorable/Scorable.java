package com.occ.scoring.scorable;

public interface Scorable<T> {
    public T getValue();
}