package com.occ.scoring.scorable;

import com.occ.scoring.scorable.Scorable;

public class ScorableString implements Scorable<String> {
    private String value;

    public ScorableString(final String string) {
        this.value = string;
    }

    public String getValue() {
        return this.value;
    }
}