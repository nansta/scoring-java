package com.occ.scoring;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import com.occ.scoring.name.NameScorer;
import com.occ.scoring.rulebook.AlphabetRuleBook;
import com.occ.scoring.rulebook.RuleBookFactory;

public final class App {
    private App() {
    }

    /**
     * Scores names in a comma delimited text file containing strings wrapped with
     * double quotes.
     * 
     * @param args The arguments of the program.
     */
    public static void main(String[] args) throws FileNotFoundException {
        if (args.length < 1) {
            throw new FileNotFoundException("Must include a file path as an arguement");
        }
        final File file = new File(args[0]);
        final Scanner sc = new Scanner(file);

        final RuleBookFactory factory = new RuleBookFactory();

        final AlphabetRuleBook rules = factory.getRuleBook("ALPHA");

        sc.useDelimiter(",");

        NameScorer scorer = new NameScorer(rules);

        while (sc.hasNext()) {
            final String name = sc.next();
            scorer.append(name);
        }

        sc.close();

        scorer.score();

        System.out.println(
            String.format("File score: %s", scorer.getScore())
        );

    }
}
