package com.occ.scoring.name;

import java.util.Arrays;

import com.occ.scoring.scorer.StringScorer;
import com.occ.scoring.scorable.Scorable;
import com.occ.scoring.rulebook.AlphabetRuleBook;

public class NameScorer extends StringScorer {
    private AlphabetRuleBook rules;
    private String[] names;
    private Integer score;
    private Integer length;

    public NameScorer(final AlphabetRuleBook ruleBook) {
        this.rules = ruleBook;
        this.score = 0;
        this.names = new String[10000];
        this.length = 0;
    }

    /**
     * Removes double quotes from name.
     * 
     * @param name The name that will have quotes removed.
     */
    private String cleanName(String name) {
        return name.replaceAll("^\"|\"$|\n", "");
    }

    /**
     * Adds name to list of names to score.
     * 
     * @param name The name that will be scored.
     */
    public void append(final String name) {
        this.names[this.length] = this.cleanName(name);
        this.length++;
    }

    /**
     * Scores names based on a given set of rules.
     * 
     * @param name The name that will be scored.
     */
    @Override
    public Integer scoreItem(final Scorable<String> name) {
        int score = 0;

        for (final char c : name.getValue().toCharArray()) {
            final int c_score = this.rules.get(c);
            score += c_score;
        }

        return score;
    }

    /**
     * Sorts the array lexicographically.
     */
    private void sortArray() {
        Arrays.sort(this.names, 0, this.length);
    }

    /**
     * Conditions to run before scoring.
     */
    private void preConditions() {
        this.sortArray();
    }

    /**
     * Resets score to zero.
     */
    public void resetScore() {
        this.score = 0;
    }

    /**
     * Scores names and sets the final score.
     */
    @Override
    public void score() {
        this.resetScore();

        this.preConditions();

        for (int i = 0; i <= this.length - 1; i++) {
            final String name = this.names[i];
            this.score += this.scoreItem(new Name(name)) * (i + 1);
        }
    }

    /**
     * Returns the final score.
     */
    @Override
    public Integer getScore() {
        return this.score;
    }
}