package com.occ.scoring.name;

import com.occ.scoring.scorable.ScorableString;

public class Name extends ScorableString {
    public Name(String item) {
        super(item);
        this.value = item;
    }

    private String value;

    public String getValue() {
        return this.value;
    }
}