package com.occ.scoring.scorer;

import com.occ.scoring.scorable.Scorable;
import com.occ.scoring.scorable.ScorableString;

public class StringScorer implements Scorer<String, Integer> {
    private String[] names = {};
    private Integer score = 0;
    private Integer length = 0;

    public StringScorer() {
        this.score = 0;
        this.names = new String[10000];
        this.length = 0;
    }

    @Override
    public void append(String item) {
        this.names[this.length] = item;
        this.length++;
    }

    @Override
    public Integer scoreItem(Scorable<String> item) {
        return 1;
    }

    /**
     * Resets score to zero.
     */
    public void resetScore() {
        this.score = 0;
    }

    @Override
    public void score() {
        this.resetScore();

        for (int i = 0; i <= this.length - 1; i++) {
            final String name = this.names[i];
            this.score += this.scoreItem(new ScorableString(name));
        }
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

}