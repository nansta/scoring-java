package com.occ.scoring.scorer;

import com.occ.scoring.scorable.Scorable;

public interface Scorer<T, S> {
    public void append(final T item);

    public S scoreItem(final Scorable<T> item);

    public void score();

    public S getScore();

    public void resetScore();
}