package com.occ.scoring.rulebook;

public class RuleBookFactory {
    public AlphabetRuleBook getRuleBook(String ruleBookType) {
        if (ruleBookType == null) {
            return null;
        }
        if (ruleBookType.equalsIgnoreCase("ALPHA")) {
            AlphabetRuleBook ruleBook = new AlphabetRuleBook();
            final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            for (int i = 1; i <= 26; i++) {
                ruleBook.put(alphabet.charAt(i - 1), i);
            }
            return ruleBook;
        }
        return null;
    }
}